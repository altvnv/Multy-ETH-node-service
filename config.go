package node

/*
Copyright 2018 Idealnaya rabota LLC
Licensed under Multy.io license.
See LICENSE for details
*/

import (
	"github.com/Appscrunch/Multy-ETH-node-service/eth"
	"github.com/Appscrunch/Multy-back/store"
)

// Configuration is a struct with all service options
type Configuration struct {
	Name        string
	GrpcPort    string
	ETHConf     eth.Conf
	ServiceInfo store.ServiceInfo
}
